# Random Quote Generator
This application will display a random quote from 'The Office'. 

## Instructions
### Clone this repository
```bash
git clone git@gitlab.com:abpluth/random-quote-generator.git
```
### Install dependencies
```bash
npm install
```
### Running the app
```bash
npm run start
```
The application should now be up and running on your machine, with swagger docs available at [localhost:3000/api](http://localhost:3000/api)

### Testing
```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```