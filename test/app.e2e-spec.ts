import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';

import { AppModule } from '../src/app.module';
import * as quotes from '../src/database/office_quotes.json';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe('/quote', () => {
    describe('/random', () => {
      test('Gets random quote', async () => {
        const response = await request(app.getHttpServer())
          .get('/quote/random')
          .expect(200);
        expect(response.body).toHaveProperty('id');
        expect(response.body).toHaveProperty('quote');
        expect(response.body).toHaveProperty('character');

        expect(quotes).toContainEqual({
          quote_id: response.body.id,
          quote: response.body.quote,
          character: response.body.character,
        });
      });

      test('Gets random quote from specific character', async () => {
        const character = 'Jim'; // test case-insensitive

        const response = await request(app.getHttpServer())
          .get('/quote/random?character=jIm') // test case-insensitive
          .expect(200);
        expect(response.body).toHaveProperty('id');
        expect(response.body).toHaveProperty('quote');
        expect(response.body.character).toEqual(character);

        expect(
          quotes.filter((quote) => quote.character === character),
        ).toContainEqual({
          quote_id: response.body.id,
          quote: response.body.quote,
          character: response.body.character,
        });
      });
    });

    describe('/:id', () => {
      test('Gets quote by id', async () => {
        const id = 8;

        const response = await request(app.getHttpServer())
          .get(`/quote/${id}`)
          .expect(200);
        expect(response.body).toHaveProperty('id');
        expect(response.body).toHaveProperty('quote');
        expect(response.body).toHaveProperty('character');

        expect(quotes.filter((quote) => quote.quote_id === id)[0]).toEqual({
          quote_id: response.body.id,
          quote: response.body.quote,
          character: response.body.character,
        });
      });
    });
  });
});
