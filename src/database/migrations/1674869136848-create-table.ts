import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
  TableIndex,
} from 'typeorm';

export class createTable1674869136848 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'quote',

        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
          },
          {
            name: 'quote',
            type: 'text',
            isNullable: false,
          },
          {
            name: 'character_id',
            type: 'integer',
            isNullable: false,
          },
        ],
      }),
    );

    await queryRunner.createTable(
      new Table({
        name: 'character',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
          },
          {
            name: 'name',
            type: 'text',
            isNullable: false,
          },
        ],
      }),
    );

    // create foreign key
    await queryRunner.createForeignKey(
      'quote',
      new TableForeignKey({
        columnNames: ['character_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'character',
        onDelete: 'CASCADE',
      }),
    );

    await queryRunner.createIndex(
      'quote',
      new TableIndex({ columnNames: ['id'] }),
    );
    await queryRunner.createIndex(
      'character',
      new TableIndex({ columnNames: ['id'] }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('quote');
  }
}
