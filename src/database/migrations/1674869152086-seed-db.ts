import { MigrationInterface, QueryRunner } from 'typeorm';

import * as quotes from '../office_quotes.json';
import { Quote } from '../../schemas/quote.entity';

export class seedDb1674869152086 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await Promise.all(
      quotes.map(({ quote_id, quote, character }) => {
        queryRunner.manager.insert(Quote, {
          id: quote_id,
          quote: quote,
          character_name: character,
        });
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await Promise.all(
      quotes.map(({ quote_id }) => {
        queryRunner.manager.delete(Quote, { id: quote_id });
      }),
    );
  }
}
