import { INestApplication } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

export default (app: INestApplication) => {
  const options = new DocumentBuilder()
    .setTitle('Random Quote Generator')
    .setDescription('Generates random quotes from The Office')
    .setVersion(process.env.API_VERSION || 'v1.0')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  return document;
};
