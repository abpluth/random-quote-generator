export default () => {
  return {
    db: {
      type: 'sqlite',
      database: ':memory:',
      synchronize: false,
      migrationsRun: true,
      entities: [__dirname + '/../**/*.entity{.ts,.js}'],
      migrations: [__dirname + '/../database/migrations/*{.ts,.js}'],
    },
  };
};
