import dbConfig from './db.config';
import swaggerConfig from './swagger.config';

export default [dbConfig, swaggerConfig];
