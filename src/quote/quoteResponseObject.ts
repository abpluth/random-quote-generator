import { ApiProperty } from '@nestjs/swagger';

export class QuoteResponseObject {
  constructor(object: any) {
    Object.assign(this, object);
  }

  @ApiProperty()
  id: number;

  @ApiProperty()
  quote: string;

  @ApiProperty()
  character: string;
}
