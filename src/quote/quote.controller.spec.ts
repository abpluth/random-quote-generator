import { Test, TestingModule } from '@nestjs/testing';
import { QuoteController } from './quote.controller';
import { QuoteService } from './quote.service';
import { HttpException, HttpStatus } from '@nestjs/common';

const quotes = [
  {
    id: 1,
    quote: 'test quote',
    character: { id: 1, name: 'some character' },
  },
  {
    id: 2,
    quote: 'test quote',
    character: { id: 2, name: 'Jim' },
  },
];

const mockQuoteService = () => ({
  getRandomQuote: jest.fn().mockImplementation((character) => {
    const filteredQuotes = character
      ? quotes.filter((quote) => quote.character.name === character)
      : quotes;

    if (!filteredQuotes.length) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
        },
        HttpStatus.NOT_FOUND,
      );
    }

    return filteredQuotes[Math.floor(Math.random() * filteredQuotes.length)];
  }),
  getQuoteById: jest.fn().mockImplementation((id) => {
    const matchingQuotes = quotes.filter((quote) => {
      return quote.id === id;
    });
    if (!matchingQuotes.length) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
        },
        HttpStatus.NOT_FOUND,
      );
    }
    return matchingQuotes[0];
  }),
});

describe('QuoteController', () => {
  let controller: QuoteController;
  let service: QuoteService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [QuoteController],
      providers: [
        {
          provide: QuoteService,
          useValue: mockQuoteService(),
        },
      ],
    }).compile();

    service = module.get<QuoteService>(QuoteService);
    controller = module.get<QuoteController>(QuoteController);
  });

  describe('randomQuote', () => {
    test('calls getRandomQuote with character param', async () => {
      const character = 'Jim';
      const response = await controller.randomQuote(character);

      expect(service.getRandomQuote).toHaveBeenCalledWith(character);
      expect(response).toEqual({
        id: expect.any(Number),
        quote: expect.any(String),
        character,
      });
      const actual = quotes.filter((quote) => quote.id === response.id)[0];
      expect(response).toEqual({
        id: actual.id,
        quote: actual.quote,
        character: actual.character.name,
      });
    });

    test('calls getRandomQuote without any params', async () => {
      const response = await controller.randomQuote();

      expect(service.getRandomQuote).toHaveBeenCalledWith(null);
      expect(response).toEqual({
        id: expect.any(Number),
        quote: expect.any(String),
        character: expect.any(String),
      });
      const actual = quotes.filter((quote) => quote.id === response.id)[0];
      expect(response).toEqual({
        id: actual.id,
        quote: actual.quote,
        character: actual.character.name,
      });
    });

    test('throws not found when called with invalid character', async () => {
      const character = 'abc';
      await expect(controller.randomQuote(character)).rejects.toThrow(
        HttpException,
      );
      expect(service.getRandomQuote).toHaveBeenCalledWith(character);
    });
  });

  describe('quoteById', () => {
    test('calls getQuoteById with specified id', async () => {
      const quoteId = 1;
      const response = await controller.quoteById(quoteId);

      const actual = quotes.filter((quote) => quote.id === quoteId)[0];
      expect(service.getQuoteById).toHaveBeenCalledWith(quoteId);
      expect(response).toEqual({
        id: actual.id,
        quote: actual.quote,
        character: actual.character.name,
      });
    });

    test('throws not found when called with invalid id', async () => {
      const quoteId = 9999;
      await expect(controller.quoteById(quoteId)).rejects.toThrow(
        HttpException,
      );
      expect(service.getQuoteById).toHaveBeenCalledWith(quoteId);
    });
  });
});
