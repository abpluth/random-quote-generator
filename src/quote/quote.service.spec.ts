import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { QuoteService } from './quote.service';
import { Quote } from '../schemas/quote.entity';

const mockQuoteRepository = () => ({
  createQueryBuilder: jest.fn().mockReturnValue({
    select: jest.fn().mockReturnThis(),
    where: jest.fn().mockReturnThis(),
    innerJoin: jest.fn().mockReturnThis(),
    orderBy: jest.fn().mockReturnThis(),
    getOneOrFail: jest
      .fn()
      .mockReturnValue({ id: 1, quote: 'abc', character: 'def' }),
  }),
});

type MockRepository<T = any> = Partial<Record<keyof Repository<T>, jest.Mock>>;

describe('QuoteService', () => {
  let service: QuoteService;
  let quoteRepo: MockRepository<Quote>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        QuoteService,
        {
          provide: getRepositoryToken(Quote),
          useValue: mockQuoteRepository(),
        },
      ],
    }).compile();

    service = module.get<QuoteService>(QuoteService);
    quoteRepo = module.get<MockRepository<Quote>>(getRepositoryToken(Quote));
  });

  afterEach(() => jest.clearAllMocks());

  describe('getRandomQuote', () => {
    describe('without query params', () => {
      test('returns random quote', async () => {
        await service.getRandomQuote();

        expect(quoteRepo.createQueryBuilder().select).toHaveBeenCalledTimes(1);
        expect(quoteRepo.createQueryBuilder().select).toHaveBeenCalledWith([
          'quote.id',
          'quote.quote',
          'character.name',
        ]);

        expect(quoteRepo.createQueryBuilder().where).toHaveBeenCalledTimes(0);

        expect(quoteRepo.createQueryBuilder().orderBy).toHaveBeenCalledTimes(1);
        expect(quoteRepo.createQueryBuilder().orderBy).toHaveBeenCalledWith(
          'RANDOM()',
        );

        expect(
          quoteRepo.createQueryBuilder().getOneOrFail,
        ).toHaveBeenCalledTimes(1);
        expect(
          quoteRepo.createQueryBuilder().getOneOrFail,
        ).toHaveBeenCalledWith();
      });
    });

    describe('with query params', () => {
      test('returns random quote', async () => {
        const character = 'Jim';
        await service.getRandomQuote(character);
        expect(quoteRepo.createQueryBuilder().select).toHaveBeenCalledTimes(1);
        expect(quoteRepo.createQueryBuilder().select).toHaveBeenCalledWith([
          'quote.id',
          'quote.quote',
          'character.name',
        ]);

        expect(quoteRepo.createQueryBuilder().where).toHaveBeenCalledTimes(1);
        expect(quoteRepo.createQueryBuilder().where).toHaveBeenCalledWith(
          'character.name LIKE :character',
          { character },
        );

        expect(quoteRepo.createQueryBuilder().orderBy).toHaveBeenCalledTimes(1);
        expect(quoteRepo.createQueryBuilder().orderBy).toHaveBeenCalledWith(
          'RANDOM()',
        );

        expect(
          quoteRepo.createQueryBuilder().getOneOrFail,
        ).toHaveBeenCalledTimes(1);
        expect(
          quoteRepo.createQueryBuilder().getOneOrFail,
        ).toHaveBeenCalledWith();
      });
    });
  });

  describe('getQuoteById', () => {
    test('returns random quote', async () => {
      const id = 123;
      await service.getQuoteById(id);

      expect(quoteRepo.createQueryBuilder().select).toHaveBeenCalledTimes(1);
      expect(quoteRepo.createQueryBuilder().select).toHaveBeenCalledWith([
        'quote.id',
        'quote.quote',
        'character.name',
      ]);

      expect(quoteRepo.createQueryBuilder().where).toHaveBeenCalledTimes(1);
      expect(quoteRepo.createQueryBuilder().where).toHaveBeenCalledWith(
        'quote.id = :id',
        { id },
      );

      expect(quoteRepo.createQueryBuilder().orderBy).toHaveBeenCalledTimes(0);

      expect(quoteRepo.createQueryBuilder().getOneOrFail).toHaveBeenCalledTimes(
        1,
      );
      expect(
        quoteRepo.createQueryBuilder().getOneOrFail,
      ).toHaveBeenCalledWith();
    });
  });
});
