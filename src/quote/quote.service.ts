import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Quote } from '../schemas/quote.entity';
import { Repository } from 'typeorm';

@Injectable()
export class QuoteService {
  constructor(
    @InjectRepository(Quote)
    private quoteRepository: Repository<Quote>,
  ) {}

  /**
   * @function getRandomQuote
   * @param {string} [character] - optional character query parameter
   * @returns Promise<Quote>
   */
  async getRandomQuote(character: string = null): Promise<Quote> {
    const query = this.quoteRepository
      .createQueryBuilder('quote')
      .select(['quote.id', 'quote.quote', 'character.name'])
      .innerJoin('quote.character', 'character')
      .orderBy('RANDOM()');

    if (character)
      query.where('character.name LIKE :character', {
        character,
      });

    return query.getOneOrFail();
  }

  /**
   * @function getQuoteById
   * @param {number} id
   * @returns Promise<Quote>
   */
  async getQuoteById(id: number): Promise<Quote> {
    return this.quoteRepository
      .createQueryBuilder('quote')
      .select(['quote.id', 'quote.quote', 'character.name'])
      .innerJoin('quote.character', 'character')
      .where('quote.id = :id', { id })
      .getOneOrFail();
  }
}
