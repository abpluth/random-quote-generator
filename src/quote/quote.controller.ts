import {
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Query,
} from '@nestjs/common';
import { QuoteResponseObject } from './quoteResponseObject';
import { QuoteService } from './quote.service';
import { ApiQuery, ApiResponse } from '@nestjs/swagger';

@Controller('quote')
@ApiResponse({
  status: HttpStatus.OK,
  description: 'Record found',
  type: QuoteResponseObject,
})
export class QuoteController {
  constructor(private readonly quoteService: QuoteService) {}

  @Get('random')
  @ApiQuery({
    name: 'character',
    type: String,
    description: 'Optional character name',
    required: false,
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'No record found matching the specified criteria',
  })
  async randomQuote(
    @Query('character') character?: string,
  ): Promise<QuoteResponseObject> {
    try {
      const response = await this.quoteService.getRandomQuote(
        character || null,
      );
      return new QuoteResponseObject({
        id: response.id,
        quote: response.quote,
        character: response.character.name,
      });
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          message: error.message,
        },
        HttpStatus.NOT_FOUND,
        { cause: error },
      );
    }
  }

  @Get(':id')
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'No record found with the specified id',
  })
  async quoteById(@Param('id') quoteId: number): Promise<QuoteResponseObject> {
    try {
      const {
        id,
        quote,
        character: { name },
      } = await this.quoteService.getQuoteById(quoteId);
      return new QuoteResponseObject({ id, quote, character: name });
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          message: error.message,
        },
        HttpStatus.NOT_FOUND,
        { cause: error },
      );
    }
  }
}
