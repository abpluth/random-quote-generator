export interface QuoteObject {
  id: number;
  quote: string;
  character: string;
}
