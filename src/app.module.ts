import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';

import { QuoteController } from './quote/quote.controller';
import { QuoteService } from './quote/quote.service';
import { Quote } from './schemas/quote.entity';
import config from './config/db.config';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [config],
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (config: ConfigService) => config.get('db'),
    }),
    TypeOrmModule.forFeature([Quote]),
  ],
  controllers: [QuoteController],
  providers: [QuoteService],
})
export class AppModule {}
